import java.net.*;

class QNSServer {
	private int serverPort;
	private int clientPort;
	private int buffer_size;
	private DatagramSocket socket;

	QNSServer(int serverPort, int clientPort) {
		this.serverPort = serverPort;
		this.clientPort = clientPort;
		this.buffer_size = 1024;
		try {
			start();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private void start() throws Exception {
		socket = new DatagramSocket(this.serverPort);
		byte[] buffer = new byte[buffer_size];
		int pos = 0;
		while(true) {
			int c = System.in.read();
			switch(c) {
				case -1:
					System.out.println("Сервер завершил работу.");
					socket.close();
					return;
				case '\r':
					break;
				case '\n':
					socket.send(new DatagramPacket(buffer, pos, InetAddress.getLocalHost(), clientPort));
					pos = 0;
					break;
				default:
					buffer[pos++] = (byte) c;
			}
		}
	}
}

class StartServer {
	public static void main(String[] args) {
		QNSServer server1 = new QNSServer(998, 999);
	}
}