import java.net.*;

class QNSClient {
	private int serverPort;
	private int clientPort;
	private int buffer_size;
	private DatagramSocket socket;

	QNSClient(int serverPort, int clientPort) {
		this.serverPort = serverPort;
		this.clientPort = clientPort;
		this.buffer_size = 1024;
		try {
			start();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private void start() throws Exception {
		socket = new DatagramSocket(this.clientPort);
		byte[] buffer = new byte[buffer_size];
		while(true) {
			DatagramPacket p = new DatagramPacket(buffer, buffer.length);
			socket.receive(p);
			System.out.println(new String(p.getData(), 0, p.getLength()));
		}
	}
}

class StartClient {
	public static void main(String[] args) {
		QNSClient client1 = new QNSClient(998, 999);
	}
}